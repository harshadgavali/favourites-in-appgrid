### Description
Starting with GNOME 40, favourite applications are only displayed in dock.
This extension makes them appear in application grid as well.
### Installation
<a href="https://extensions.gnome.org/extension/4485/favourites-in-appgrid/">
<img src="https://github.com/andyholmes/gnome-shell-extensions-badge/raw/master/get-it-on-ego.svg" alt="Get it on EGO" width="200" />
</a>

#### From git repo
```
git clone https://gitlab.gnome.org/harshadgavali/favourites-in-appgrid.git/
cd favourites-in-appgrid
make
```

### Screenshot
![Screenshot](./screenshot.png)